using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Spine.Unity;
using Spine;
using Spine.Unity.AttachmentTools;

public class ColorSpine : MonoBehaviour
{
    //=====================================================================
    //				      VARIABLES 
    //=====================================================================
    //===== SINGLETON =====
    public static ColorSpine m_Instance;
    //===== STRUCT =====
    public enum e_BodyType
    {
        HAIR = 0,
        BODY,
        FACE,
        HAND,
        PANTS,
        LEG
    }
    //===== PUBLIC =====
    public Skeleton m_Skeleton;
    public Attachment m_Attachment;
    public SkeletonAnimation m_SkeletonAnimation;

    public Material m_SourceMaterial;
    [SpineAttachment(currentSkinOnly: true, slotField: "head")]
    public List<string> m_Head;
    [SpineAttachment(currentSkinOnly: true, slotField: "hair")]
    public List<string> m_Hair;


    [SpineAttachment(currentSkinOnly: true, slotField: "Body_Skin")]
    public List<string> m_BodySkin;
    [SpineAttachment(currentSkinOnly: true, slotField: "Hipp_Skin")]
    public List<string> m_HippSkin;
    [SpineAttachment(currentSkinOnly: true, slotField: "HandL1_Skin")]
    public List<string> m_HandL1_Skin;
    [SpineAttachment(currentSkinOnly: true, slotField: "HandL2_Skin")]
    public List<string> m_HandL2_Skin;
    [SpineAttachment(currentSkinOnly: true, slotField: "HandR1_Skin")]
    public List<string> m_HandR1_Skin;
    [SpineAttachment(currentSkinOnly: true, slotField: "HandR2_Skin")]
    public List<string> m_HandR2_Skin;


    [SpineAttachment(currentSkinOnly: true, slotField: "LegL1_Skin")]
    public List<string> m_LegL1_Skin;
    [SpineAttachment(currentSkinOnly: true, slotField: "LegL2_Skin")]
    public List<string> m_LegL2_Skin;
    [SpineAttachment(currentSkinOnly: true, slotField: "LegL3_Skin")]
    public List<string> m_LegL3_Skin;
    [SpineAttachment(currentSkinOnly: true, slotField: "LegR1_Skin")]
    public List<string> m_LegR1_Skin;
    [SpineAttachment(currentSkinOnly: true, slotField: "LegR2_Skin")]
    public List<string> m_LegR2_Skin;
    [SpineAttachment(currentSkinOnly: true, slotField: "LegR3_Skin")]
    public List<string> m_LegR3_Skin;













    public Sprite m_BodySprite;
    //===== PRIVATES =====
    CosmeticManager m_Cosmetic;
    //=====================================================================
    //				MONOBEHAVIOUR METHOD 
    //=====================================================================
    void Awake()
    {
        m_Instance = this;
    }

    void Start()
    {
        //f_Combine();
        m_Cosmetic = CosmeticManager.instance;
    }

    void Update()
    {
        f_Combine();
    }
    //=====================================================================
    //				    OTHER METHOD
    //=====================================================================

    public void f_Combine()
    {
        //m_Skeleton = m_SkeletonAnimation.skeleton;
        //m_SourceMaterial = m_SkeletonAnimation.skeletonDataAsset.atlasAssets[0].PrimaryMaterial;
        ////Debug.Log(m_Skeleton.FindSlotIndex("Body_Skin"));
        //Debug.Log(m_Skeleton.GetAttachment(m_Skeleton.FindSlotIndex("Body_Skin"), "mc_cowo_2nd_skin/body 2nd"));
        ////m_Attachment = m_Skeleton.GetAttachment(m_Skeleton.FindSlotIndex("Body_Skin"), "mc_cowo_2nd_skin/body 2nd");
        ////Attachment m_NewAttachment = m_Attachment.GetRemappedClone(
        ////    m_BodySprite,
        ////    m_SourceMaterial,
        ////    true,
        ////    true,
        ////    false,
        ////    true,
        ////    false);
        //m_Skeleton.FindSlot("Body_Skin").Attachment = null;
        //Skin m_Skin = new Skin("Naama");
        //m_Skin.SetAttachment(m_Skeleton.FindSlotIndex("Body_Skin"), "mc_cowo_2nd_skin/body 2nd", m_Attachment);

        //m_Skeleton.SetSkin(m_Skin);
        //m_Skeleton.SetSlotsToSetupPose();
        m_SkeletonAnimation.skeleton.SetAttachment("Body_Skin", m_BodySkin[m_Cosmetic.m_IndexClothes]);
        m_SkeletonAnimation.skeleton.SetAttachment("HandL1_Skin", m_HandL1_Skin[m_Cosmetic.m_IndexClothes]);
        m_SkeletonAnimation.skeleton.SetAttachment("HandL2_Skin", m_HandL2_Skin[m_Cosmetic.m_IndexClothes]);
        m_SkeletonAnimation.skeleton.SetAttachment("HandR1_Skin", m_HandR1_Skin[m_Cosmetic.m_IndexClothes]);
        m_SkeletonAnimation.skeleton.SetAttachment("HandR2_Skin", m_HandR2_Skin[m_Cosmetic.m_IndexClothes]);
        m_SkeletonAnimation.skeleton.SetAttachment("Hipp_Skin", m_HippSkin[m_Cosmetic.m_IndexPants]);
        m_SkeletonAnimation.skeleton.SetAttachment("LegL1_Skin", m_LegL1_Skin[m_Cosmetic.m_IndexPants]);
        m_SkeletonAnimation.skeleton.SetAttachment("LegL2_Skin", m_LegL2_Skin[m_Cosmetic.m_IndexPants]);
        m_SkeletonAnimation.skeleton.SetAttachment("LegL3_Skin", m_LegL3_Skin[m_Cosmetic.m_IndexPants]);
        m_SkeletonAnimation.skeleton.SetAttachment("LegR1_Skin", m_LegR1_Skin[m_Cosmetic.m_IndexPants]);
        m_SkeletonAnimation.skeleton.SetAttachment("LegR2_Skin", m_LegR2_Skin[m_Cosmetic.m_IndexPants]);
        m_SkeletonAnimation.skeleton.SetAttachment("LegR3_Skin", m_LegR3_Skin[m_Cosmetic.m_IndexPants]);



        m_SkeletonAnimation.skeleton.FindSlot("head").SetColor(m_Cosmetic.m_ColorGameObjectSkin.color);
        m_SkeletonAnimation.skeleton.FindSlot("hair").SetColor(m_Cosmetic.m_ColorGameObjectHair.color);
        m_SkeletonAnimation.skeleton.FindSlot("Body_Skin").SetColor(m_Cosmetic.m_ColorGameObjectClothes.color);
        m_SkeletonAnimation.skeleton.FindSlot("HandL1_Skin").SetColor(m_Cosmetic.m_ColorGameObjectClothes.color);
        m_SkeletonAnimation.skeleton.FindSlot("HandL2_Skin").SetColor(m_Cosmetic.m_ColorGameObjectClothes.color);
        m_SkeletonAnimation.skeleton.FindSlot("HandR1_Skin").SetColor(m_Cosmetic.m_ColorGameObjectClothes.color);
        m_SkeletonAnimation.skeleton.FindSlot("HandR2_Skin").SetColor(m_Cosmetic.m_ColorGameObjectClothes.color);


        m_SkeletonAnimation.skeleton.FindSlot("Hipp_Skin").SetColor(m_Cosmetic.m_ColorGameObjectPants.color);
        m_SkeletonAnimation.skeleton.FindSlot("LegL1_Skin").SetColor(m_Cosmetic.m_ColorGameObjectPants.color);
        m_SkeletonAnimation.skeleton.FindSlot("LegL2_Skin").SetColor(m_Cosmetic.m_ColorGameObjectPants.color);
        m_SkeletonAnimation.skeleton.FindSlot("LegL3_Skin").SetColor(m_Cosmetic.m_ColorGameObjectPants.color);
        m_SkeletonAnimation.skeleton.FindSlot("LegR1_Skin").SetColor(m_Cosmetic.m_ColorGameObjectPants.color);
        m_SkeletonAnimation.skeleton.FindSlot("LegR2_Skin").SetColor(m_Cosmetic.m_ColorGameObjectPants.color);
        m_SkeletonAnimation.skeleton.FindSlot("LegR3_Skin").SetColor(m_Cosmetic.m_ColorGameObjectPants.color);


        Debug.Log("Spine" + m_SkeletonAnimation.skeleton.FindSlot("hair").GetColor());
        Debug.Log("Color" + m_Cosmetic.m_ColorGameObjectHair.color);
        //m_SkeletonAnimation.AnimationState.Apply(m_Skeleton);
        //m_SkeletonAnimation.AnimationState.Apply(m_Skeleton);
        //m_SkeletonAnimation.AnimationState.Apply(m_Skeleton);
        //m_SkeletonAnimation.Update(0);
    }

    public void f_NextSkin(int p_Index)
    {
        if (p_Index == (int)e_BodyType.HAIR)
        {

        }
        else if (p_Index == 1)
        {

        }
    }
}
