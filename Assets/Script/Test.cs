using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Test : MonoBehaviour
{
    // Start is called before the first frame update
    public Text m_TextMoney;
    public Text m_TextAddMoney;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        m_TextMoney.text = PlayFabManager.instance.m_Money.ToString();
        m_TextAddMoney.text = PlayFabManager.instance.m_InputMoney.ToString();

    }


    public void f_AddMoney()
    {
        PlayFabManager.instance.m_InputMoney += 50;

    }
    public void f_SubtMoney()
    {

        if (PlayFabManager.instance.m_InputMoney > 0)
        {
            PlayFabManager.instance.m_InputMoney -= 50;
        }
    }



}
