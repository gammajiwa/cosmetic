using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameObjectFriendRequest : MonoBehaviour
{
    // Start is called before the first frame update
    public Text m_name;
    public string m_IdFriend;


    public void f_Decline()
    {
        PlayFabManager.instance.f_DeclineFriend(m_IdFriend);
        Destroy(gameObject);
        // PlayFabManager.instance.f_Refresh();


    }
    public void f_Accept()
    {
        PlayFabManager.instance.f_AcceptFriend(m_IdFriend);
        Destroy(gameObject);
        // PlayFabManager.instance.f_Refresh();

    }



}
