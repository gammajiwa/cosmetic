using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CosmeticManager : MonoBehaviour
{
    public static CosmeticManager instance;

    public int m_IndexHair;
    public int m_IndexClothes;
    public int m_IndexPants;
    public int m_IndexShoes;

    public int m_ColorChangeIndex;

    public Image m_ColorGameObjectSkin;
    public Image m_ColorGameObjectHair;
    public Image m_ColorGameObjectClothes;
    public Image m_ColorGameObjectPants;
    public Image m_ColorGameObjectShoes;



    public Image[] m_Color;
    public GameObject m_GameobjectColorPalette;
    public GameObject[] m_GameobjectClothes;
    public GameObject[] m_GameobjectPants;


    public Vector2[] m_InnerCrossPos;
    public Vector2[] m_OuterCrossPos;
    public ColorSelector m_Select;




    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            // DontDestroyOnLoad(gameObject);
        }
        else Destroy(gameObject);
    }

    private void Update()
    {
        switch (m_ColorChangeIndex)
        {
            case 0:
                f_GetColor(m_ColorGameObjectSkin);
                break;
            case 1:
                f_GetColor(m_ColorGameObjectHair);
                break;
            case 2:
                f_GetColor(m_ColorGameObjectClothes);
                break;
            case 3:
                f_GetColor(m_ColorGameObjectPants);
                break;
            case 4:
                f_GetColor(m_ColorGameObjectShoes);
                break;
        }

    }



    void f_GetColor(Image m_parentColor)
    {
        m_parentColor.color = m_Color[m_ColorChangeIndex].color;
        Image[] m_gameObjectColors = m_parentColor.GetComponentsInChildren<Image>();
        foreach (var m_gc in m_gameObjectColors)
        {
            m_gc.color = m_parentColor.color;

        }
    }

    void f_LoopIndex(GameObject[] m_GameObject, int m_Index, int m_Switch, bool m_Add)
    {
        int m_Slot;
        m_Slot = m_GameObject.Length;

        if (m_Add)
        {
            m_Index++;
        }
        else m_Index--;


        if (m_Index == m_Slot)
        {
            m_Index = 0;
        }
        else if (m_Index < 0)
        {
            m_Index = m_Slot - 1;
        }


        switch (m_Switch)
        {
            case 2:
                m_IndexClothes = m_Index;
                break;
            case 3:
                m_IndexPants = m_Index;
                break;
        }
    }


    public void f_AddIndex(int m_id)
    {

        switch (m_id)
        {
            case 1:

                break;
            case 2:
                f_LoopIndex(m_GameobjectClothes, m_IndexClothes, 2, true);
                f_SetActived(m_GameobjectClothes, m_IndexClothes);
                break;
            case 3:
                f_LoopIndex(m_GameobjectPants, m_IndexPants, 3, true);
                f_SetActived(m_GameobjectPants, m_IndexPants);
                break;
            case 4:
                m_IndexShoes++;
                break;
        }
    }

    public void f_SubIndex(int m_id)
    {
        switch (m_id)
        {
            case 1:
                m_IndexHair--;
                break;
            case 2:
                f_LoopIndex(m_GameobjectClothes, m_IndexClothes, 2, false);
                f_SetActived(m_GameobjectClothes, m_IndexClothes);
                break;
            case 3:
                f_LoopIndex(m_GameobjectPants, m_IndexPants, 3, false);
                f_SetActived(m_GameobjectPants, m_IndexPants);
                break;
            case 4:
                m_IndexShoes--;
                break;
        }
    }


    void f_SetActived(GameObject[] m_gameObject, int m_index)
    {
        for (int i = 0; i < m_gameObject.Length; i++)
        {
            m_gameObject[i].SetActive(false);
        }
        m_gameObject[m_index].SetActive(true);
    }


    public void f_ColorChange(int m_id)
    {
        m_GameobjectColorPalette.SetActive(!m_GameobjectColorPalette.activeSelf);
        m_ColorChangeIndex = m_id;
        m_Select.f_PosInnerCrossHair(new Vector2(m_InnerCrossPos[m_ColorChangeIndex].x, m_InnerCrossPos[m_ColorChangeIndex].y));
        m_Select.f_PosOuterCrossHair(new Vector2(m_OuterCrossPos[m_ColorChangeIndex].x, m_OuterCrossPos[m_ColorChangeIndex].y));

        // m_GameobjectColorPalette.transform.position = new Vector2(m_GameobjectColorPalette.transform.position.x, m_Color[m_ColorChangeIndex].transform.position.y);

    }








}
