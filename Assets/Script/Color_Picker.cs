using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using UnityEngine.Events;

[Serializable]
public class ColorEvent : UnityEvent<Color> { }
public class Color_Picker : MonoBehaviour
{
    RectTransform m_Rect;
    public TextMeshProUGUI m_dbug;
    public ColorEvent[] onPreview;
    public ColorEvent[] onSelect;

    Texture2D m_Texture;
    private void Start()
    {
        m_Rect = GetComponent<RectTransform>();
        m_Texture = GetComponent<Image>().mainTexture as Texture2D;

    }
    private void Update()
    {
        f_pickColor();
    }

    void f_pickColor()
    {
        if (RectTransformUtility.RectangleContainsScreenPoint(m_Rect, Input.mousePosition))
        {
            Vector2 m_delta;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(m_Rect, Input.mousePosition, null, out m_delta);


            string dbug = "mouse pos=" + Input.mousePosition;
            // dbug += "<br>delta" + m_delta;

            float m_width = m_Rect.rect.width;
            float m_height = m_Rect.rect.height;
            m_delta += new Vector2(m_width * .5f, m_height * .5f);
            // dbug += "<br>offsitedelta" + m_delta;

            float m_x = Mathf.Clamp(m_delta.x / m_width, 0, 1f);
            float m_y = Mathf.Clamp(m_delta.y / m_height, 0, 1f);
            // dbug += "<br>x=" + m_x + "y=" + m_y;

            int m_texX = Mathf.RoundToInt(m_x * m_Texture.width);
            int m_texY = Mathf.RoundToInt(m_y * m_Texture.height);
            // dbug += "<br>texX=" + m_texX + "texY=" + m_texY;
            Color m_color = m_Texture.GetPixel(m_texX, m_texY);
            // m_dbug.color = m_color;
            // m_dbug.text = dbug;
            onPreview[CosmeticManager.instance.m_ColorChangeIndex]?.Invoke(m_color);

            if (Input.GetMouseButtonDown(0))
            {
                onSelect[CosmeticManager.instance.m_ColorChangeIndex]?.Invoke(m_color);
                CosmeticManager.instance.m_GameobjectColorPalette.SetActive(false);
            }

        }

    }
}
