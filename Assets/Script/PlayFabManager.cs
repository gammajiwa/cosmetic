using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine.UI;



public class PlayFabManager : MonoBehaviour
{
    public static PlayFabManager instance;
    public int m_Money;
    public int m_InputMoney;


    public InputField m_EmailInput;
    public InputField m_InputUserName;

    public InputField m_InputFriend;
    public InputField m_PasswordInput;
    public Text m_Message;
    public Text m_PlayerName;

    public GameObject m_Ui;
    public GameObject m_Login;
    public GameObject m_Submitname;
    public GameObject m_TabRequetFriend;
    public GameObject m_PreFabTabRequetFriend;
    public GameObject m_TabMyFriend;
    public GameObject m_PreFabTabMyFriend;






    string m_name = null;

    string m_ID;
    public List<FriendInfo> m_RequestFriends = new List<FriendInfo>();
    public List<FriendInfo> m_AllFriendsList = null;
    public List<FriendInfo> m_MyFriend = new List<FriendInfo>();

    bool m_login = false;



    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else Destroy(gameObject);


        m_Ui.SetActive(false);
        m_Login.SetActive(true);
    }
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {
        m_PlayerName.text = m_name + " Money";


    }






    void f_OnError(PlayFabError p_Error)
    {
        Debug.Log(p_Error.GenerateErrorReport());
        m_Message.text = p_Error.ErrorMessage;
    }


    public void f_AddMoney()
    {
        var m_Request = new AddUserVirtualCurrencyRequest
        {
            VirtualCurrency = "RP",
            Amount = m_InputMoney
        };
        PlayFabClientAPI.AddUserVirtualCurrency(m_Request, f_OnMoneySend, f_OnError);
    }
    void f_OnMoneySend(ModifyUserVirtualCurrencyResult p_Result)
    {
        Debug.Log("Add Money Success");
        m_Message.text = "Add Money Success";
        f_GetMoney();
    }

    public void f_GetMoney()
    {
        PlayFabClientAPI.GetUserInventory(new GetUserInventoryRequest(), f_OnMoneyReceived, f_OnError);


    }

    void f_OnMoneyReceived(GetUserInventoryResult p_result)
    {

        m_Money = p_result.VirtualCurrency["RP"];
    }






    public void f_Register()
    {
        if (m_PasswordInput.text.Length < 6)
        {
            m_Message.text = "Password too short";
            return;
        }
        var m_Request = new RegisterPlayFabUserRequest
        {
            Email = m_EmailInput.text,
            Password = m_PasswordInput.text,
            RequireBothUsernameAndEmail = false
        };
        PlayFabClientAPI.RegisterPlayFabUser(m_Request, f_OnRegister, f_OnError);
    }

    void f_OnRegister(RegisterPlayFabUserResult p_Result)
    {
        m_Message.text = "Registered Success";
        Debug.Log("Registered Success");
        m_EmailInput.text = "";
        m_PasswordInput.text = "";
    }






    public void f_LoginEmail()
    {
        var m_Request = new LoginWithEmailAddressRequest
        {
            Email = m_EmailInput.text,
            Password = m_PasswordInput.text,
            InfoRequestParameters = new GetPlayerCombinedInfoRequestParams
            {
                GetPlayerProfile = true
            }
        };
        PlayFabClientAPI.LoginWithEmailAddress(m_Request, f_OnLoginSuccess, f_OnError);
    }

    void f_OnLoginSuccess(LoginResult p_Result)
    {
        f_GetMoney();
        Debug.Log("Success login");
        m_Message.text = "Logged in!";
        m_Login.SetActive(false);
        f_GetFriendsInfo();
        m_login = true;

        if (p_Result.InfoResultPayload.PlayerProfile != null)
        {
            m_name = p_Result.InfoResultPayload.PlayerProfile.DisplayName;
        }

        if (m_name == null)
            m_Submitname.SetActive(true);
        else
            m_Ui.SetActive(true);
    }


    public void f_PassReset()
    {
        var m_Request = new SendAccountRecoveryEmailRequest
        {
            Email = m_EmailInput.text,
            TitleId = "A111E"
        };
        PlayFabClientAPI.SendAccountRecoveryEmail(m_Request, f_OnPassReset, f_OnError);
    }

    void f_OnPassReset(SendAccountRecoveryEmailResult p_Result)
    {
        m_Message.text = "Passwod reset email sent";
        Debug.Log("Passwod Reset Email Sent");
    }





    public void f_SubmitName()
    {
        var m_Request = new UpdateUserTitleDisplayNameRequest
        {
            DisplayName = m_InputUserName.text
        };
        PlayFabClientAPI.UpdateUserTitleDisplayName(m_Request, f_OnSubmitName, f_OnError);
        m_name = m_InputUserName.text;
    }

    void f_OnSubmitName(UpdateUserTitleDisplayNameResult p_Result)
    {

        Debug.Log("Submit Name Success");
        m_Ui.SetActive(true);
        m_Submitname.SetActive(false);

    }




    public void f_AddFriend()
    {
        var m_Request = new GetAccountInfoRequest
        {
            Email = m_InputFriend.text
        };

        PlayFabClientAPI.GetAccountInfo(m_Request, f_OnAddFriend, f_OnError);
    }


    void f_OnAddFriend(GetAccountInfoResult p_Result)
    {
        m_ID = p_Result.AccountInfo.PlayFabId;
        Debug.Log("Get User ID");
        f_AddFriendCloudScript();
    }

    void f_AddFriendCloudScript()
    {
        var m_Request = new ExecuteCloudScriptRequest
        {
            FunctionName = "SendFriendRequest",
            FunctionParameter = new
            {
                FriendPlayFabId = m_ID
            }
        };
        PlayFabClientAPI.ExecuteCloudScript(m_Request, f_ResultCloudScript, f_OnError);
    }

    void f_ResultCloudScript(ExecuteCloudScriptResult p_Result)
    {
        // m_Message.text = p_Result.FunctionResult.ToString();
        Debug.Log("Cloud Script Success");

    }




    // public void f_GetAccountInfo(int p_index)
    // {
    //     var m_Request = new GetAccountInfoRequest
    //     {
    //         Email = m_InputFriend.text
    //     };

    //     PlayFabClientAPI.GetAccountInfo(m_Request, f_OngetInfo, f_OnError);
    // }


    // void f_OngetInfo(GetAccountInfoResult p_Result)
    // {
    //     m_ID = p_Result.AccountInfo.PlayFabId;
    //     Debug.Log("Get User ID");
    //     f_AddFriendCloudScript();
    // }


    public void f_GetFriendsInfo()
    {
        PlayFabClientAPI.GetFriendsList(new GetFriendsListRequest
        {
            IncludeSteamFriends = false,
            IncludeFacebookFriends = false,
            XboxToken = null
        },
        result =>
        {
            m_AllFriendsList = result.Friends;
            foreach (PlayFab.ClientModels.FriendInfo m_Friend in m_AllFriendsList)
            {
                foreach (string tag in m_Friend.Tags)
                {
                    switch (tag)
                    {
                        case "requester":
                            m_RequestFriends.Add(m_Friend);
                            f_AddTabFriend
                            (
                                m_Friend.TitleDisplayName,
                                m_Friend.FriendPlayFabId,
                                m_PreFabTabRequetFriend,
                                m_TabRequetFriend
                                );
                            break;
                        case "confirmed":
                            m_MyFriend.Add(m_Friend);
                            f_AddTabFriend(
                              m_Friend.TitleDisplayName,
                              m_Friend.FriendPlayFabId,
                              m_PreFabTabMyFriend,
                              m_TabMyFriend
                              );
                            break;
                        default:
                            break;
                    }
                }
            }
        }, f_OnError);
    }


    public void f_AcceptFriend(string p_IDFriend)
    {
        var m_Request = new ExecuteCloudScriptRequest
        {
            FunctionName = "AcceptFriendRequest",
            FunctionParameter = new
            {
                FriendPlayFabId = p_IDFriend
            }
        };
        PlayFabClientAPI.ExecuteCloudScript(m_Request, f_ResultCloudScript, f_OnError);
    }

    public void f_DeclineFriend(string p_IDFriend)
    {
        var m_Request = new ExecuteCloudScriptRequest
        {
            FunctionName = "DenyFriendRequest",
            FunctionParameter = new
            {
                FriendPlayFabId = p_IDFriend
            }
        };
        PlayFabClientAPI.ExecuteCloudScript(m_Request, f_ResultCloudScript, f_OnError);
    }


    void f_AddTabFriend(string p_Name, string p_Id, GameObject p_Prefab, GameObject p_Pos)
    {
        GameObject m_Tab = Instantiate(p_Prefab);
        m_Tab.transform.parent = p_Pos.transform;
        GameObjectFriendRequest m_MyTab = m_Tab.GetComponent<GameObjectFriendRequest>();
        m_MyTab.m_name.text = p_Name;
        m_MyTab.m_IdFriend = p_Id;

    }

    public void f_Refresh()
    {
        m_RequestFriends.Clear();
        m_AllFriendsList.Clear();
        m_MyFriend.Clear();
        GameObject[] m_tab = GameObject.FindGameObjectsWithTag("TabFriend");
        foreach (GameObject item in m_tab)
        {
            Destroy(item);
        }
        f_GetFriendsInfo();
    }


}


