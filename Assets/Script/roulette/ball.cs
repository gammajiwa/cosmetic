using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ball : MonoBehaviour
{
    // Start is called before the first frame update


    // private void OnTriggerEnter2D(Collider2D other)
    // {
    //     if (other.gameObject.tag == "box" && Roulette.instance.m_BatResult == 1)
    //     {
    //         if (Roulette.instance.m_Time <= 0.01 && Roulette.instance.m_SpeedBall <= 0)
    //         {
    //             Roulette.instance.m_SpeedBall = 0;
    //             gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, other.gameObject.transform.position, 0.4f * Time.deltaTime);
    //             if (Vector3.Distance(gameObject.transform.position, other.gameObject.transform.position) <= 0.1)
    //             {
    //                 StartCoroutine(f_Stop(1.5f));
    //             }
    //         }
    //     }
    // }
    // private void OnTriggerExit2D(Collider2D other)
    // {
    //     if (other.gameObject.tag == "box" && Roulette.instance.m_BatResult == 2 && Roulette.instance.m_SpeedBall <= 0)
    //     {
    //         if (Roulette.instance.m_Time <= 0.01)
    //         {
    //             // if (Vector3.Distance(gameObject.transform.position, other.gameObject.transform.position) >= 0.2)
    //             // {

    //             StartCoroutine(f_Stop(2f));
    //             // }
    //         }
    //     }
    // }




    private void OnTriggerEnter2D(Collider2D other)
    {
        var slot = other.gameObject.GetComponent<SlotRoulette>();
        if (other.gameObject.tag == "box" && slot.m_idSlot == Roulette.instance.m_BatResult)
        {
            if (Roulette.instance.m_Time <= 0.01 && Roulette.instance.m_SpeedBall >= -0.09)
            {
                Roulette.instance.f_ResultColor((int)slot.m_Color);
                Roulette.instance.f_TextResult((string)slot.m_Color.ToString());
                Roulette.instance.m_SpeedBall = 0;
                gameObject.transform.position = new Vector2(other.transform.position.x, other.transform.position.y);
                StartCoroutine(f_Stop(2.5f));

            }
        }
    }


    IEnumerator f_Stop(float m_WaitTime)
    {
        yield return new WaitForSeconds(m_WaitTime);
        Roulette.instance.m_SpeedRotate = 0;
        Roulette.instance.f_Result();
    }





}
