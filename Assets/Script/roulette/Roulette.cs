using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Roulette : MonoBehaviour
{
    public static Roulette instance;
    public GameObject m_Ball;
    public GameObject m_BtnBet;
    public GameObject m_BtnPlay;
    public GameObject m_BtnReset;
    public GameObject m_ResultUI;
    public GameObject m_Rules;

    public SlotBet[] m_SlotBet;


    public float m_SpeedBall;
    public float m_SpeedRotate;
    public int m_BatResult;
    public int m_BatResultColor;
    public List<int> m_Inputs = new List<int>();
    public float m_Time;
    public float m_Subt;
    public bool m_ColorChoice;
    public bool m_MultiplesTwelve;
    public int[] m_BetMoney;
    public int m_indexMoney = 0;

    public Text m_TextMoney;
    public Text m_TextResultMoney;
    public Text m_TextResult;
    public Text m_TextWin;
    public Text m_TextBet;
    public Text m_TextTotalBet;


    public int m_RewardValue
    {
        get
        {
            return m_Reward;
        }
        set
        {
            m_Reward = value;
        }
    }

    int m_Reward;
    int m_RewardMoney;



    float m_ResetSpeedBall;
    float m_ResetSpeedRotate;


    public enum e_State
    {
        opening,
        play,
        result,
    }


    public e_State m_State;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else Destroy(gameObject);
    }

    private void Start()
    {
        m_ResetSpeedBall = m_SpeedBall;
        m_ResetSpeedRotate = m_SpeedRotate;
    }
    void Update()
    {
        if (m_State == e_State.play)
        {
            transform.Rotate(new Vector3(0, 0, m_SpeedRotate));
            m_Ball.transform.Rotate(new Vector3(0, 0, m_SpeedBall));
            if (m_Time <= 0.01f)
            {
                if (m_SpeedRotate > 0.06)
                    m_SpeedRotate = m_SpeedRotate - 0.05f * Time.deltaTime;


                if (m_SpeedBall < -0.08)
                    m_SpeedBall = m_SpeedBall + 0.1f * Time.deltaTime;
            }
            f_SubtTime();
        }
        m_TextMoney.text = m_BetMoney[m_indexMoney].ToString("");

    }

    float f_SubtTime()
    {
        if (m_Time > 0.01f)
        {
            m_Time = m_Time - m_Subt * Time.deltaTime;
        }
        return m_Time;
    }


    // float f_SinePower(float m_speed)
    // {
    //     return Mathf.Sin(f_SubtTime() * m_speed);
    // }


    public void f_Result()
    {
        for (int i = 0; i < m_Inputs.Count; i++)
        {
            if (m_Inputs[i] == m_BatResult && m_Inputs[i] <= 36)
            {
                f_Reward(36);
            }

            if (m_ColorChoice)
            {
                if (m_Inputs[i] == m_BatResultColor)
                {
                    f_Reward(2);
                }
            }

            if (m_MultiplesTwelve)
            {
                if (m_Inputs[i] == f_MultiplesTwelve(m_BatResult))
                {
                    f_Reward(3);
                }
            }


        }



        m_ResultUI.SetActive(true);
        m_State = e_State.result;
        m_BtnReset.gameObject.SetActive(true);
        m_SpeedBall = m_ResetSpeedBall;
        m_SpeedRotate = m_ResetSpeedRotate;
        m_ColorChoice = false;
        m_MultiplesTwelve = false;
        m_BatResultColor = 0;

    }

    public void f_Start()
    {
        if (m_Inputs.Count != 0)
        {
            m_State = e_State.play;
            m_BatResult = Random.Range(0, 37);
            m_Time = Random.Range(4, 5);
            m_BtnPlay.gameObject.SetActive(false);
            f_ReduceMoney();
            m_BtnBet.gameObject.SetActive(false);
            f_textBet();
        }
    }

    public void f_AddBetNumber(int p_Value)
    {
        // m_BtnBet.gameObject.SetActive(false);
        foreach (var m_Slots in m_SlotBet)
        {
            if (m_Slots.m_id == p_Value)
            {
                if (m_Slots.m_SlotChoice.activeInHierarchy)
                {
                    f_CancelBet(m_Slots);
                    m_Inputs.Remove(p_Value);
                }
                else
                {
                    m_Inputs.Add(p_Value);
                    m_Slots.m_SlotChoice.SetActive(true);
                }
            }
        }
    }





    // public void f_AddBetColor(int p_Value)
    // {
    //     m_ColorChoice = true;
    //     m_Inputs = p_Value;
    //     if (p_Value == 36)
    //     {
    //         m_TextBet.text = "Red";
    //     }
    //     else if(p_Value == 37)
    //     {
    //         m_TextBet.text = "Black";
    //     }
    // }

    // public void f_AddMultiplesTwelve(int p_Value)
    // {
    //     m_MultiplesTwelve = true;
    //     m_Inputs = p_Value;
    //     // m_BtnBet.gameObject.SetActive(false);
    //     switch (p_Value)
    //     {
    //         case 1:
    //             m_TextBet.text = "1-12";
    //             break;
    //         case 2:
    //             m_TextBet.text = "13-24";
    //             break;
    //         case 3:
    //             m_TextBet.text = "24-36";
    //             break;
    //     }
    // }


    public void f_Reset()
    {

        m_State = e_State.opening;
        m_BatResult = 0;
        f_InputClear();
        m_BtnBet.gameObject.SetActive(true);
        m_BtnReset.gameObject.SetActive(false);
        m_BtnPlay.gameObject.SetActive(true);
        m_ResultUI.SetActive(false);
        m_TextBet.text = "";
        m_RewardMoney = 0;
        m_TextResult.text = "";
        m_TextResultMoney.text = "0";
        f_lose();
    }

    public void f_ChangeBetMoney(int p_Index)
    {
        // m_indexMoney = p_Index == 1 ? m_indexMoney++ : m_indexMoney--;
        if (p_Index == 1)
        {
            if (m_indexMoney != m_BetMoney.Length - 1)
                m_indexMoney++;
        }
        else if (m_indexMoney > 0)
        {
            m_indexMoney--;

        }
    }
    void f_ReduceMoney()
    {
        int money = m_BetMoney[m_indexMoney] * m_Inputs.Count;
        Debug.Log("-" + money);
        m_TextTotalBet.text = "-" + money.ToString();
    }

    void f_Reward(int p_Reward)
    {


        m_RewardValue = p_Reward;
        int m_Money = m_RewardMoney + m_BetMoney[m_indexMoney] * m_Reward;
        Debug.Log("+" + m_BetMoney[m_indexMoney] * m_Reward);
        m_RewardMoney = m_Money;


        m_TextResultMoney.text = "+" + m_RewardMoney.ToString();

        if (m_RewardMoney > m_BetMoney[m_indexMoney] * m_Inputs.Count)
        {
            m_TextWin.text = "WIN!!";
            m_TextWin.color = Color.yellow;
            Debug.Log("Win");
        }
        else
        {
            f_lose();
            Debug.Log("lose");
        }

    }

    void f_lose()
    {
        m_TextWin.text = "LOSE";
        m_TextWin.color = Color.red;

    }


    int f_MultiplesTwelve(int p_index)
    {
        if (p_index <= 12)
        {
            return 39;
        }
        else if (p_index >= 13 && p_index <= 24)
        {
            return 40;
        }
        else return 41;
    }


    public void f_ResultColor(int p_Color)
    {
        m_BatResultColor = p_Color;
        // Debug.Log(m_BatResultColor);
    }

    public void f_TextResult(string p_Result)
    {
        m_TextResult.text = m_BatResult.ToString();

        if (m_MultiplesTwelve)
        {
            // int index = f_MultiplesTwelve(m_BatResult);
            switch (f_MultiplesTwelve(m_BatResult))
            {
                case 39:
                    m_TextResult.text += ", 1-12";
                    break;
                case 40:
                    m_TextResult.text += ", 13-24";
                    break;
                case 41:
                    m_TextResult.text += ", 24-36";
                    break;
            }
        }
        if (m_ColorChoice)
        {
            m_TextResult.text += ", " + p_Result;
        }
    }

    void f_CancelBet(SlotBet p_Slots)
    {
        p_Slots.m_SlotChoice.SetActive(false);
    }

    void f_InputClear()
    {
        m_Inputs.Clear();
        foreach (var m_Slots in m_SlotBet)
        {
            m_Slots.m_SlotChoice.SetActive(false);
        }
    }


    public void f_Rules()
    {
        m_Rules.SetActive(!m_Rules.activeSelf);
    }


    void f_textBet()
    {

        foreach (var item in m_Inputs)
        {
            switch (item)
            {
                case 37:
                    m_TextBet.text += "RED" + ", ";
                    m_ColorChoice = true;
                    break;
                case 38:
                    m_TextBet.text += "BLACK" + ", ";
                    m_ColorChoice = true;
                    break;
                case 39:
                    m_TextBet.text += "1-12" + ", ";
                    m_MultiplesTwelve = true;
                    break;
                case 40:
                    m_TextBet.text += "13-24" + ", ";
                    m_MultiplesTwelve = true;
                    break;
                case 41:
                    m_TextBet.text += "24-36" + ", ";
                    m_MultiplesTwelve = true;
                    break;
                default:
                    m_TextBet.text += item.ToString() + ", ";
                    break;
            }
        }
    }
}
